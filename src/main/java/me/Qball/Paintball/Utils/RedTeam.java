package me.Qball.Paintball.Utils;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class RedTeam {
    public ItemStack getRedHelmet()
    {
        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) helmet.getItemMeta();
        armorMeta.setColor(Color.RED);
        helmet.setItemMeta(armorMeta);
        return helmet;
    }
    public ItemStack getRedChest()
    {
        ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) chest.getItemMeta();
        armorMeta.setColor(Color.RED);
        chest.setItemMeta(armorMeta);
        return chest;
    }
    public ItemStack getRedLeg()
    {
        ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) leg.getItemMeta();
        armorMeta.setColor(Color.RED);
        leg.setItemMeta(armorMeta);
        return leg;
    }
    public ItemStack getRedBoot()
    {
        ItemStack boot = new ItemStack(Material.LEATHER_BOOTS,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) boot.getItemMeta();
        armorMeta.setColor(Color.RED);
        boot.setItemMeta(armorMeta);
        return boot;
    }



}
