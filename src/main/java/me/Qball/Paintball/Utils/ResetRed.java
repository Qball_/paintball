package me.Qball.Paintball.Utils;


import org.bukkit.entity.Player;

public class ResetRed {
    public void reset(Player p)
    {
        RedTeam red = new RedTeam();
        p.getInventory().setHelmet(red.getRedHelmet());
        p.getInventory().setChestplate(red.getRedChest());
        p.getInventory().setLeggings(red.getRedLeg());
        p.getInventory().setBoots(red.getRedBoot());
    }
}
