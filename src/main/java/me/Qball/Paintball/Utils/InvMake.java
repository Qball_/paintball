package me.Qball.Paintball.Utils;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

import java.util.ArrayList;


public class InvMake {
    public ItemStack getGun()
    {
        ItemStack gun = new ItemStack(Material.IRON_BARDING,1);
        ItemMeta meta = gun.getItemMeta();
        meta.setDisplayName("Paintball Rifle");
        ArrayList<String> lore = new ArrayList<String>();
        lore.add("Hit the opponent 2 times to stun them with this");
        meta.setLore(lore);
        gun.setItemMeta(meta);
        return gun;
    }
    public ItemStack getBlueWool()
    {
        Wool blue = new Wool(Material.WOOL);
        blue.setColor(DyeColor.BLUE);
        ItemStack wool = blue.toItemStack(1);
        ItemMeta meta = wool.getItemMeta();
        meta.setDisplayName("Blue Team");
        wool.setItemMeta(meta);
        return wool;
    }
    public ItemStack getRedWool()
    {
        Wool red = new Wool(Material.WOOL);
        red.setColor(DyeColor.RED);
        ItemStack wool = red.toItemStack(1);
        ItemMeta meta = wool.getItemMeta();
        meta.setDisplayName("Red Team");
        wool.setItemMeta(meta);
        return wool;
    }

}
