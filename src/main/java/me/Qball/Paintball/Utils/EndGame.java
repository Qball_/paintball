package me.Qball.Paintball.Utils;


import me.Qball.Paintball.Core;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class EndGame{
    public Core core = new Core();
    public void endGame()
    {

        while(Core.inGame)
        {
            if(Core.deadBlue.size() == Core.blueTeam.size())
            {
                Bukkit.broadcastMessage("Game over Red team won the game");
            new BukkitRunnable() {
                public void run() {

                    String spawn = core.getConfig().getString(
                            "HubSpawn");
                    String[] reSpawn = spawn.split(",");
                    String world = reSpawn[0];
                    String x = reSpawn[1];
                    String y = reSpawn[2];
                    String z = reSpawn[3];
                    String pitch = reSpawn[4];
                    String yaw = reSpawn[5];
                    final Location loc = new Location(
                            Bukkit.getWorld(world),
                            Integer.parseInt(x),
                            Integer.parseInt(y),
                            Integer.parseInt(z),
                            Float.parseFloat(pitch),
                            Float.parseFloat(yaw));
                    for (int i = 0; i < +Core.players.size(); i++) {
                        Player p = Bukkit.getServer().getPlayer(Core.players.get(i));
                        p.teleport(loc);
                        Core.players.clear();;
                        Core.deadBlue.clear();
                        Core.blueTeam.clear();
                        Core.deadRed.clear();
                        Core.redTeam.clear();
                        Core.hits.clear();
                    }

                }
            }.runTaskLater(core, 100);
            Core.inGame = false;
        }
        else if(Core.redTeam.size() ==  Core.deadRed.size())
            {
                Bukkit.broadcastMessage("Game over Blue team won the game");
            new BukkitRunnable() {
                public void run() {

                    String spawn = core.getConfig().getString(
                            "HubSpawn");
                    String[] reSpawn = spawn.split(",");
                    String world = reSpawn[0];
                    String x = reSpawn[1];
                    String y = reSpawn[2];
                    String z = reSpawn[3];
                    String pitch = reSpawn[4];
                    String yaw = reSpawn[5];
                    final Location loc = new Location(
                            Bukkit.getWorld(world),
                            Integer.parseInt(x),
                            Integer.parseInt(y),
                            Integer.parseInt(z),
                            Float.parseFloat(pitch),
                            Float.parseFloat(yaw));
                    for (int i = 0; i < +Core.players.size(); i++) {
                        Player p = Bukkit.getServer().getPlayer(Core.players.get(i));
                        p.teleport(loc);
                        Core.players.clear();;
                        Core.deadBlue.clear();
                        Core.blueTeam.clear();
                        Core.deadRed.clear();
                        Core.redTeam.clear();
                        Core.hits.clear();
                    }

                }
            }.runTaskLater(core, 100);
            Core.inGame = false;
        }
        }
    }

}
