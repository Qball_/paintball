package me.Qball.Paintball.Utils;


import me.Qball.Paintball.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.*;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

import static org.bukkit.potion.PotionType.WATER_BREATHING;

public class GameStart {
    public void gameStart()
    {
        Core core = new Core();
        if(Core.players.size() ==10)
        {
            ResetBlue blue = new ResetBlue();
            ResetRed red = new ResetRed();
            for(UUID uuid : Core.blueTeam)
            {
                Player p = Bukkit.getPlayer(uuid);
                blue.reset(p);
                ItemStack pot = new ItemStack(Material.SPLASH_POTION,4);
                PotionMeta meta = (PotionMeta) pot.getItemMeta();
                meta.setBasePotionData(new PotionData(WATER_BREATHING));
                pot.setItemMeta(meta);
            }
            for(UUID uuid : Core.redTeam)
            {
                Player p = Bukkit.getPlayer(uuid);
                red.reset(p);
                ItemStack pot = new ItemStack(Material.SPLASH_POTION,4);
                PotionMeta meta = (PotionMeta) pot.getItemMeta();
                meta.setBasePotionData(new PotionData(WATER_BREATHING));
                pot.setItemMeta(meta);
            }
            Core.inGame = true;
            String spawn = core.getConfig().getString("GameSpawn");
            String[] reSpawn = spawn.split(",");
            String world = reSpawn[0];
            String x = reSpawn[1];
            String y = reSpawn[2];
            String z = reSpawn[3];
            String pitch = reSpawn[4];
            String yaw = reSpawn[5];
            Bukkit.getServer().broadcastMessage(ChatColor.RED+"The game has started there is a 10 second grace period to run about");
            final Location loc = new Location(
                    Bukkit.getWorld(world),
                    Integer.parseInt(x),
                    Integer.parseInt(y),
                    Integer.parseInt(z),
                    Float.parseFloat(pitch),
                    Float.parseFloat(yaw));
            for (int i = 0; i < +Core.players.size(); i++) {
                Player p = Bukkit.getServer().getPlayer(Core.players.get(i));
                p.teleport(loc);
            }
            new BukkitRunnable() {
                public void run() {

                    Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + "Grace period is over. Lets game begin");

                }
            }.runTaskLater(core, 200);
        }

    }
}
