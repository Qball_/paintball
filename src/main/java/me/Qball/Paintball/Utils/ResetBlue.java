package me.Qball.Paintball.Utils;


import org.bukkit.entity.Player;

public class ResetBlue {
    public void reset(Player p)
     {
        BlueTeam blue = new BlueTeam();
        p.getInventory().setHelmet(blue.getBlueHelmet());
        p.getInventory().setChestplate(blue.getBlueChest());
        p.getInventory().setLeggings(blue.getBlueLeg());
        p.getInventory().setBoots(blue.getBlueBoot());
    }

}
