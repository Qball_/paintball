package me.Qball.Paintball.Utils;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class BlueTeam {
    public ItemStack getBlueHelmet()
    {
        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) helmet.getItemMeta();
        armorMeta.setColor(Color.BLUE);
        helmet.setItemMeta(armorMeta);
        return helmet;
    }
    public ItemStack getBlueChest()
    {
        ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) chest.getItemMeta();
        armorMeta.setColor(Color.BLUE);
        chest.setItemMeta(armorMeta);
        return chest;
    }
    public ItemStack getBlueLeg()
    {
        ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) leg.getItemMeta();
        armorMeta.setColor(Color.BLUE);
        leg.setItemMeta(armorMeta);
        return leg;
    }
    public ItemStack getBlueBoot()
    {
        ItemStack boot = new ItemStack(Material.LEATHER_BOOTS,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) boot.getItemMeta();
        armorMeta.setColor(Color.BLUE);
        boot.setItemMeta(armorMeta);
        return boot;
    }



}
