package me.Qball.Paintball.Utils;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.block.Skull;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class DeadTeam {
    public ItemStack getPurpleHelmet()
    {
        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) helmet.getItemMeta();
        armorMeta.setColor(Color.PURPLE);
        helmet.setItemMeta(armorMeta);
        return helmet;
    }
    public ItemStack getPurpleChest()
    {
        ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) chest.getItemMeta();
        armorMeta.setColor(Color.PURPLE);
        chest.setItemMeta(armorMeta);
        return chest;
    }
    public ItemStack getPurpleLeg()
    {
        ItemStack leg = new ItemStack(Material.LEATHER_LEGGINGS,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) leg.getItemMeta();
        armorMeta.setColor(Color.PURPLE);
        leg.setItemMeta(armorMeta);
        return leg;
    }
    public ItemStack getPurpleBoot()
    {
        ItemStack boot = new ItemStack(Material.LEATHER_BOOTS,1);
        LeatherArmorMeta armorMeta = (LeatherArmorMeta) boot.getItemMeta();
        armorMeta.setColor(Color.PURPLE);
        boot.setItemMeta(armorMeta);
        return boot;
    }
    public ItemStack getSkull()
    {
        ItemStack skeletonSkull = new ItemStack(Material.SKULL_ITEM, 1, (short)0);
        return skeletonSkull;
    }



}
