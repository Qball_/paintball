package me.Qball.Paintball.Listeners;

import me.Qball.Paintball.Core;
import me.Qball.Paintball.Utils.ResetBlue;
import me.Qball.Paintball.Utils.ResetRed;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffectType;


public class SplashPotionEvent implements Listener {
    @EventHandler
    public void onSplashPot(PotionSplashEvent e) {
        if (e.getPotion().getShooter() instanceof Player) {
            Player p = (Player) e.getPotion().getShooter();
            if (e.getPotion().getEffects().contains(PotionEffectType.WATER_BREATHING) && Core.hits.get(p.getUniqueId()) > 4) {

                for (Entity ent : e.getAffectedEntities()) {
                    if (Core.players.contains(ent)){
                        e.setCancelled(true);
                        Core.hits.put(ent.getUniqueId(), 0);
                        if(Core.redTeam.contains(ent.getUniqueId()))
                        {
                            ResetRed reset = new ResetRed();
                            reset.reset((Player)ent);
                            Core.deadRed.remove(ent.getUniqueId());
                        }
                        else if(Core.blueTeam.contains(ent.getUniqueId())) {
                            ResetBlue reset = new ResetBlue();
                            reset.reset((Player)ent);
                            Core.deadBlue.remove(ent.getUniqueId());
                        }
                    }

                }

            }

        }
    }
}

