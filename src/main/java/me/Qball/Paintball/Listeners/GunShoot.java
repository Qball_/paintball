package me.Qball.Paintball.Listeners;


import me.Qball.Paintball.Core;
import me.Qball.Paintball.Utils.InvMake;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class GunShoot implements Listener {
    @EventHandler
    public void shootGun(PlayerInteractEvent e)
    {
        InvMake inv = new InvMake();
        if (Core.players.contains(e.getPlayer().getUniqueId()))
        {
            if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)||
                    e.getAction().equals(Action.RIGHT_CLICK_AIR)
                            &&e.getItem().getType().equals(inv.getGun().getType())
                            &&e.getItem().getItemMeta().getLore().equals(inv.getGun().getItemMeta().getLore()))
            {

             e.getPlayer().launchProjectile(Snowball.class);
            }
        }
    }

}
