package me.Qball.Paintball.Listeners;

import me.Qball.Paintball.Core;
import me.Qball.Paintball.Utils.DeadTeam;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class DamageListener implements Listener{
    @EventHandler
    public void cancelDamage(EntityDamageEvent e)
    {
        if(e.getEntity() instanceof Player)
        {
            final Player p = (Player) e.getEntity();
            DeadTeam dead = new DeadTeam();
            if (Core.players.contains(e.getEntity().getUniqueId())) {
                e.setCancelled(true);
                if (Core.hits.get(e.getEntity().getUniqueId()) <= 4) {
                    if (e.getCause().equals(EntityType.SNOWBALL)) {
                        if (Core.hits.get(e.getEntity().getUniqueId()) == 0) {
                            p.getInventory().setChestplate(dead.getPurpleChest());
                            Core.hits.put(p.getUniqueId(),1);
                        }
                       else if (Core.hits.get(e.getEntity().getUniqueId()) == 1) {
                            p.getInventory().setLeggings(dead.getPurpleLeg());
                            Core.hits.put(p.getUniqueId(),2);
                        }
                        else if (Core.hits.get(e.getEntity().getUniqueId()) == 2) {
                            p.getInventory().setBoots(dead.getPurpleBoot());
                            Core.hits.put(p.getUniqueId(),3);
                        }
                       else if (Core.hits.get(e.getEntity().getUniqueId()) == 0) {
                            p.getInventory().setHelmet(dead.getSkull());
                            Core.hits.put(p.getUniqueId(),4);
                        }

                    }
                }
            }
        }
    }

}
