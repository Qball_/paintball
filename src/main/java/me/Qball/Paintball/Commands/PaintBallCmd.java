package me.Qball.Paintball.Commands;


import me.Qball.Paintball.Core;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PaintBallCmd implements CommandExecutor {

    private final Core plugin;

    public PaintBallCmd(Core plugin)
    {
        this.plugin = plugin;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd , String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("paintball")) {
            if(sender instanceof Player) {
                Player p = (Player) sender;
                if (args.length == 0) {
                    p.sendMessage(ChatColor.RED + "You must do /b <join>, <leave> <team>");
                }
                if(args.length>=1) {
                    if (args[0].equalsIgnoreCase("join")) {
                        if (!Core.inGame) {
                            if (!Core.players.contains(p.getUniqueId())) {
                                Core.players.add(p.getUniqueId());
                            } else {
                                p.sendMessage(ChatColor.GREEN + "You have already joined");
                        }
                        } else {
                            p.sendMessage(ChatColor.RED + "A game is already in progress");
                        }
                    }
                    else if (args[0].equalsIgnoreCase("leave")) {
                        if (Core.inGame) {
                            if (Core.players.contains(p.getUniqueId())) {
                                p.sendMessage(ChatColor.GREEN + "You have left the game queue");
                            } else {
                                p.sendMessage(ChatColor.RED + "You aren't in the game queue");
                            }
                        }
                        else
                        {
                            p.sendMessage(ChatColor.RED +"You cannot leave once the game has started");
                        }
                        }
                    else if(args[0].equalsIgnoreCase("set"))
                    {
                        if(args[1].equalsIgnoreCase("hubspawn"))
                        {
                            if(p.hasPermission("paintball.set.spawn"))
                            {
                                String world = p.getWorld().toString();
                                String x = String.valueOf(p.getLocation().getX());
                                String y = String.valueOf(p.getLocation().getY());
                                String z = String.valueOf(p.getLocation().getZ());
                                String pitch = String.valueOf(p.getLocation().getPitch());
                                String yaw = String.valueOf(p.getLocation().getYaw());
                                String location = world + ","+x+","+y+","+z+","+pitch+","+yaw;
                                plugin.getConfig().set("HubSpawn", location);
                                plugin.saveConfig();
                                plugin.reloadConfig();
                                p.sendMessage(ChatColor.GREEN+"You have set the Hub Spawn");
                            }
                        }
                        else if(args[1].equalsIgnoreCase("gamSpawn"))
                        {
                            if(p.hasPermission("paintball.set.spawn"))
                            {
                                String world = p.getWorld().toString();
                                String x = String.valueOf(p.getLocation().getX());
                                String y = String.valueOf(p.getLocation().getY());
                                String z = String.valueOf(p.getLocation().getZ());
                                String pitch = String.valueOf(p.getLocation().getPitch());
                                String yaw = String.valueOf(p.getLocation().getYaw());
                                String location = world + ","+x+","+y+","+z+","+pitch+","+yaw;
                                plugin.getConfig().set("GameSpawn", location);
                                plugin.saveConfig();
                                plugin.reloadConfig();
                                p.sendMessage(ChatColor.GREEN+"You have set the Game Spawn");
                            }
                        }
                    }

                    else if(args[0].equalsIgnoreCase("team"));
                    {
                        p.sendMessage(ChatColor.RED+"You must do team <red or blue>");
                        if (args.length>=2)
                        {
                            if(args[1].equalsIgnoreCase("blue"))
                            {
                                if(Core.blueTeam.size() > 5 && !Core.inGame)
                                {
                                    if(Core.redTeam.contains(p.getUniqueId()))
                                    {
                                        Core.redTeam.remove(p.getUniqueId());
                                        Core.blueTeam.add(p.getUniqueId());
                                        p.sendMessage(ChatColor.GREEN + "You have joined the blue team");
                                }
                                else
                                    {
                                        Core.blueTeam.add(p.getUniqueId());
                                        p.sendMessage(ChatColor.GREEN + "You have joined the blue team");
                                    }
                                }
                                }
                           else if(args[1].equalsIgnoreCase("red"))
                            {
                                if(Core.redTeam.size() > 5 && !Core.inGame)
                                {
                                    if(Core.blueTeam.contains(p.getUniqueId()))
                                    {
                                        Core.blueTeam.remove(p.getUniqueId());
                                        Core.redTeam.add(p.getUniqueId());
                                        p.sendMessage(ChatColor.GREEN+ "You have joined the red team");
                                    }
                                    else
                                    {
                                        Core.redTeam.add(p.getUniqueId());
                                        p.sendMessage(ChatColor.GREEN+ "You have joined the red team");
                                    }
                                }
                            }
                        }
                    }


                }
            }
        }
        return true;
    }

}
