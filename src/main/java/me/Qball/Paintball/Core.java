package me.Qball.Paintball;

import me.Qball.Paintball.Commands.PaintBallCmd;
import me.Qball.Paintball.Listeners.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;


public class Core extends JavaPlugin {
    public static ArrayList<UUID> players = new ArrayList<UUID>();
    public static boolean inGame = false;
    public static HashMap<UUID,Integer> hits = new HashMap<>();
    public static ArrayList<UUID> blueTeam = new ArrayList<>();
    public static ArrayList<UUID> redTeam = new ArrayList<>();
    public static ArrayList<UUID> deadRed = new ArrayList<>();
    public static ArrayList<UUID> deadBlue = new ArrayList<>();
    public void onEnable()

    {
        Bukkit.getServer().getPluginManager().registerEvents(new DamageListener(),this);
        Bukkit.getServer().getPluginManager().registerEvents(new NoBlockBreak(),this);
        Bukkit.getServer().getPluginManager().registerEvents(new GunShoot(),this);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerMove(),this);
        Bukkit.getServer().getPluginManager().registerEvents(new SplashPotionEvent(),this);
        this.getCommand("paintball").setExecutor(new PaintBallCmd(this));
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
    }
    public void onDisable()
    {}

}
